public with sharing class StackExh_InlineAccountController {

	public Account acc {get;set;}
	public List<Task> tasks {get;set;}
	public List<Task> closedTasks {get;set;}
	public List<Task> tasksLimited {get;set;}
	public List<Task> closedTasksLimited {get;set;}
	
	public List<Contact> cons {get;set;}
	
	public StackExh_InlineAccountController(ApexPages.StandardController con){
		acc = (Account) con.getRecord();
		//Get Only Staff Contacts
		cons=[SELECT id FROM Contact WHERE AccountId=: acc.Id];
		
		
		tasks=new List<Task>();
		closedTasks=new List<Task>();
		tasksLimited=new List<Task>();
		closedTasksLimited=new List<Task>();
	}

	
	
	
	//Get the School Account Information
	public Account getAccount() {
	    return [SELECT id, name FROM Account WHERE Id=:System.currentPageReference().getParameters().get('id')];
	}
	
	//Get Open Tasks for School Staff
	public List<Task> getTasks() {
	
	    tasks= [SELECT id, whoid, description, subject, type, whatid, activitydate, ownerid, status, priority, accountid FROM task WHERE WhoId in :cons AND IsClosed=false ORDER BY activitydate DESC];
	    return tasks;
	}
	
	//Get Closed Tasks for School Staff
	public List<Task> getClosedTasks() {
	
	    closedTasks= [SELECT id, whoid, description, subject, type, whatid, activitydate, ownerid, status, priority, accountid FROM task WHERE WhoId in :cons AND IsClosed=true ORDER BY activitydate DESC];
	    return closedTasks;
	}
	
	//Get Open Tasks for School Staff LIMIT 10
	public List<Task> getTasksLimited() {
	
	    tasksLimited= [SELECT id, whoid, description, subject, type, whatid, activitydate, ownerid, status, priority, accountid FROM task WHERE WhoId in :cons AND IsClosed=false ORDER BY activitydate DESC LIMIT 10];
	    return tasksLimited;
	}
	
	//Get Closed Tasks for School Staff LIMIT 10
	public List<Task> getClosedTasksLimited() {
	
	    closedTasksLimited= [SELECT id, whoid, description, subject, type, whatid, activitydate, ownerid, status, priority, accountid FROM task WHERE WhoId in :cons AND IsClosed=true ORDER BY activitydate DESC LIMIT 10];
	    return closedTasksLimited;
	}
}
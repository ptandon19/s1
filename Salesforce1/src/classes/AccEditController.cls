public with sharing class  AccEditController {

    public String ssn { get; set; }
    public String id{get; set;}
public String name{get; set;}
public String screenType { get; set; }   

public Account account{get; set;}   
public boolean isDisable{get; set;}
public boolean editDisabled{get; set;}

public list<Account> accList {get;set;}
public static String accId {get;set;}

//this is used to identify wheteher to redirect the page or not if any exception comes while processing.
public boolean pageMsgRendered { get; set; }

public String xy {get;set;}


public AccEditController (ApexPages.StandardController con){
	account = (Account) con.getRecord();
	accList = [Select Id,Name from Account];

  screenType = ApexPages.currentPage().getParameters().get('type');

  id =ApexPages.currentPage().getParameters().get('id');
  
	xy = 'Hello';

}

private void getAccount(){
	try{
      //Getting the name and SSN of the client based upon its record Id.
      account = [Select name, id, PerName__c from account where id = :id limit 1];
      name = account.name;

  }catch(Exception ex){
      if(ApexPages.getMessages()==null){
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No record found for this Client'));
      }

  }
}

public PageReference showAccount(){
	PageReference ref = new PageReference('/'+accId);
	ref.setRedirect(true);
	return ref;
}


/**
* This method updates the client with the new SSN entered by User.
*/
public void savePerName() {

    try{

        update account;

        pageMsgRendered = false;
        //system.debug('after update'+ acct );

    }catch(Exception e){
        system.debug('exception at'+e);
        if(ApexPages.getMessages()==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'An exception occurred while updating.'));
        }
        pageMsgRendered = true;          
    }

 }
 
 public PageReference cancelAcc(){
 	getAccount();
 	PageReference ref = ApexPages.currentPage();
 	ref.setredirect(true);
 	return ref;
 }


 /**
 * To make edit button enable or disable.
 */
 public void makeEditable() {

    isDisable = false;
 }
 
  public Pagereference action1(){
        xy='Hello Hello';
    return null;
    }
}
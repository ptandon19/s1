public class LinkedInIntegrationController {
 
	//Pass in the endpoint to be used
	public String responseBody{get;set;}
	public String authCode{get;set;}
	public String authorizationCodeURI{get;set;}
	public String body{get;set;}
	public JSONWrapper jsonWrapObj {get;set;}
	public String text{get;set;}
	public JSONPostWrapper jsonPostWrpObj{get;set;}
 
	public LinkedInIntegrationController(){
 
		authCode = ApexPages.currentPage().getParameters().get('code');
		authorizationCodeURI = 'https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=752habxlowbco9&state=DCEEFWF45453sdffef424&redirect_uri=https://c.ap1.visual.force.com/apex/Oauth_LinkedInIntegration';
		HttpResponse resToken;
		if(authCode != NULL){
	 
			//POST Request by using the authorization code
			 Http hToken = new Http();
			 HttpRequest reqToken = new HttpRequest();
			 reqToken.setEndpoint('https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code='+authCode+'&redirect_uri=https://c.ap1.visual.force.com/apex/Oauth_LinkedInIntegration&client_id=752habxlowbco9&client_secret=jcfCFrI80P2S6oxV');
			 reqToken.setMethod('POST');
			 resToken = hToken.send(reqToken);
			 body = resToken.getBody();
			 
			jsonWrapObj = (JSONWrapper)JSON.deserialize(body, JSONWrapper.class);
			 
			//GET Request by using the access token
			 Http h=new Http();
			 HttpRequest req = new HttpRequest();
			 req.setEndpoint('https://api.linkedin.com/v1/people/~?format=json&oauth2_access_token='+jsonWrapObj.access_token);
			 req.setMethod('GET');
			 req.setHeader('Content-Type','application/json');
			 HttpResponse res = h.send(req);
			 responseBody = res.getBody();
			 text = responseBody;
		}
	}
	
	//POST Request by using the access token
	public void post(){ 
		Http h=new Http();
		HttpRequest req = new HttpRequest();
		req.setEndpoint('https://api.linkedin.com/v1/people/~/shares?format=json&oauth2_access_token='+jsonWrapObj.access_token);
		req.setMethod('POST');
		req.setHeader('Content-Type','application/json');
		req.setBody('{"comment": "'+text+'","visibility": {"code": "anyone"}}');
		HttpResponse res = h.send(req);
		responseBody = res.getBody();
		 
		jsonPostWrpObj = (JSONPostWrapper)JSON.deserialize(body, JSONPostWrapper.class);
	}
	
	//DELETE Request by using the access token
	public void postDelete(){
		Http h=new Http();
		HttpRequest req = new HttpRequest();
		req.setEndpoint('https://api.linkedin.com/v1/people/~/current-status?format=json&oauth2_access_token='+jsonWrapObj.access_token);
		req.setMethod('DELETE');
		req.setHeader('Content-Type','application/json');
		HttpResponse res = h.send(req);
		responseBody = res.getBody();
	}
 
	public class JSONWrapper{
		public Integer expires_in;
		public String access_token;
	}
	
	public class JSONPostWrapper{
		public String updateKey;
	}
}
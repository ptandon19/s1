public with sharing class Poly_ColoursController {
	
	public List<Poly_ColourClassBase> colours{get;set;}
 
    public Poly_ColoursController(){
        colours = new List<Poly_ColourClassBase>();
 
        colours.add(new Poly_ColourClassBase());
        colours.add(new Poly_Orange());
        colours.add(new Poly_Green());
    }

}
global with sharing class Angular_AccListController {
	public String accountName { get; set; }
    public Angular_AccListController() { } // empty constructor
    
    @RemoteAction
    global static List<Account> getAccounts() {
        List<Account> accountList = [SELECT Id, Name, Phone, Type, NumberOfEmployees,BillingStreet 
                   FROM Account];
        return accountList;
    }
    
    @RemoteAction
    global static Account getAccount(String accId) {
        Account acc = [SELECT Id, Name, Phone, Type, NumberOfEmployees,BillingStreet 
                   FROM Account where Id =: accId];
        return acc;
    }
    
    @RemoteAction
    global static Account updateAccount(String acc) {
    	Account accObj = (Account)JSON.deserialize(acc, Account.class);
        upsert accObj;
        return accObj;
    }
    
    @RemoteAction
    global static void deleteAccount(String accId) {
        delete [Select Id from Account where Id =: accId];
    }
}